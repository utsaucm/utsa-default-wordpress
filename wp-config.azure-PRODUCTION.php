<?php

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', getenv ('DB_NAME'));

/** MySQL database username */
define('DB_USER', getenv ('DB_USER'));

/** MySQL database password */
define('DB_PASSWORD', getenv ('DB_PASSWORD'));

/** MySQL hostname */
define('DB_HOST', getenv ('DB_HOST'));

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'zL&4]Lc6P|#Y8*Qk0:wo-:+I;UcdYkI=O9AdeIt!n+&O*X*!/4p&5P>v8-?Z.2_b');
define('SECURE_AUTH_KEY',  '/p)~+K#^YuHN1_Ch:-j8K8a+@#;wF)wAiniU`^&AsPfY#G<K6vOQI^+xj>Feo`YU');
define('LOGGED_IN_KEY',    ':J,Ej~``F{-o9~]SSq^/S-o@jI*0748>wwn.TS?i@cx=;vRbn-=9}QkmEHP^&}0W');
define('NONCE_KEY',        'AigRt|Ibz*|ENkZH^&OB0eko0!HDTShh1E#Q#J9<MCX]a~rSW*^@>Lg~WJ}u^)rz');
define('AUTH_SALT',        'f!o@U[x<6_*hqdu;f7>2@%~kwUl$G@vnF_6S[ne{iiD(=*3,L-ZQtF>9r;*oBZi!');
define('SECURE_AUTH_SALT', '<Gb0_mD=P6kI)qH]<Ave{tn9#UEEZ,^nN;<%aolRi<.G0DqR^KT(c+]@oh}MwFj+');
define('LOGGED_IN_SALT',   'YG3o+]g`#8L7I|0F(ho?qPY+y@G[L,hbMAO~t) l:h C<6s35Gq,<sLoZ2;dsGhb');
define('NONCE_SALT',       '<)PSNq[C;.ag}9 ?v:u-vf;t.hT]-9*XW:G=biCqmvux?$Ek+g,-}d]Ic@^V$ P#');

define('WP_DEBUG', false);
define('WP_DEBUG_LOG', false);
define('WP_DEBUG_DISPLAY', false);
