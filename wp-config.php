<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */


 // Support multiple environments
 // set the config file based on current environment
 $config_file = '';

 if  ((strpos(getenv('WP_ENV'),'STAGING') !== false) || (strpos(getenv('WP_ENV'),'PRODUCTION' )!== false )) {
   if  (strpos(getenv('WP_ENV'),'PRODUCTION') !== false) {
     $config_file = 'wp-config.azure-PRODUCTION.php';
   } else {
     $config_file = 'wp-config.azure-STAGING.php';
   }
 } else {
	 $config_file = 'wp-config.local.php';
 }

 $path = dirname(__FILE__) . '/';
 error_log('WP_ENV', false);
 error_log(getenv('WP_ENV'), false);

 error_log($path, false);
 error_log($config_file, false);

 if (file_exists($path . $config_file)) {
     // include the config file if it exists, otherwise WP is going to fail
     require_once $path . $config_file;
 }


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */


/* That's all, stop editing! Happy blogging. */


#define('WP_HOME', 'http://' . $_SERVER['HTTP_HOST']);
#define('WP_SITEURL', 'http://' . $_SERVER['HTTP_HOST']);
#define('WP_CONTENT_URL', '/wp-content');
#define('DOMAIN_CURRENT_SITE', $_SERVER['HTTP_HOST']);

//Relative URLs for swapping across app service deployment slots
define('WP_HOME', 'http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING));
define('WP_SITEURL', 'http://'. filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING));
define('WP_CONTENT_URL', '/wp-content');
define('DOMAIN_CURRENT_SITE', filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING));

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
